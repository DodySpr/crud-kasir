<!DOCTYPE html>
<html lang="en">
<head>
  <title>Latihan CRUD Laravel</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
  <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
</head>
<body>

<div class="container pt-5">
  <h2>CRUD Kasir</h2>
  <p></p>            
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Jenis Kelamin</th>
        <th>Email</th>
        <th>Alamat</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    @foreach ($admins as $admin )
      <tr>
        <td>{{$loop->index+1}}</td>
        <td>{{$admin->nama}}</td>
        <td>{{$admin->jekel}}</td>
        <td>{{$admin->email}}</td>
        <td>{{$admin->alamat}}</td>
        <td>
        <button class="btn btn-sm btn-warning">Edit</button>
         <button class="btn btn-sm btn-danger">Delete</button>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

</body>
</html>